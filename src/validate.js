"use strict"
import {
    required,
    alpha_spaces,
    regex,
    confirmed,
    email,
    min,
} from "vee-validate/dist/rules"
import { extend } from "vee-validate"

extend("required", {
    ...required,
    message:"votre {_field_} est obligatoire",
})

extend("alpha_spaces", {
    ...alpha_spaces,
    message: "votre {_field_} doit contenir uniquement des lettres et des espaces",
})

extend("email", {
    ...email,
    message:"votre {_field_} doit être une adresse valide",
})

extend("confirmed", {
    ...confirmed,
    message: "ce  {_field_} confirmation does not match",
})

extend("regex", {
    ...regex,
    message: "votre {_field_} doit contenir au moins une lettre minuscule et majuscule, un chiffre et un caractère particulier",
})

extend("min", {
    ...min,
    message: "votre {_field_} doit contenir 8 caractères au minimum",
})
