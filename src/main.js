'use strict'
import Vue from 'vue'
import Buefy from 'buefy'
import { library } from '@fortawesome/fontawesome-svg-core'
import 'buefy/dist/buefy.css'
import './validate'
import App from './App.vue'
import router from './router'

import {faCheck, faCheckCircle, faInfoCircle, faExclamationTriangle, faExclamationCircle,
    faArrowUp, faAngleRight, faAngleLeft, faAngleDown, faEye, faEyeSlash, faCaretDown, faCaretUp, faUpload} from "@fortawesome/free-solid-svg-icons"
import {  } from "@fortawesome/free-brands-svg-icons"
import { } from "@fortawesome/free-regular-svg-icons"
import { FontAwesomeIcon } from "@fortawesome/vue-fontawesome"
library.add(faCheck, faCheckCircle, faInfoCircle, faExclamationTriangle, faExclamationCircle,
    faArrowUp, faAngleRight, faAngleLeft, faAngleDown, faEye, faEyeSlash, faCaretDown, faCaretUp, faUpload)
Vue.component("vue-fontawesome", FontAwesomeIcon)


Vue.config.productionTip = false

Vue.use(Buefy, {
  defaultIconComponent: "vue-fontawesome",
  defaultIconPack: "fas",
  defaultContainerElement: "#content",
})

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
